package actions;

import com.opensymphony.xwork2.ActionSupport;
import modele.GestionUtilisateur;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.ApplicationAware;
import org.apache.struts2.interceptor.SessionAware;

import java.util.Map;

public class Environement extends ActionSupport implements ApplicationAware {

    public static final String GESTION_UTILISATEUR = "GestionUtilisateur";
    private Map<String, Object> application;
    private GestionUtilisateur gestion ;




    public GestionUtilisateur getGestion() {
        return gestion;
    }

    @Override
    public void setApplication(Map<String, Object> map) {
        application = map;
        if (null ==  application.get(GESTION_UTILISATEUR)){
            gestion = new GestionUtilisateur();
            application.put(GESTION_UTILISATEUR,gestion);
        }
        else{
            gestion = (GestionUtilisateur) application.get(GESTION_UTILISATEUR);
        }

    }



}
