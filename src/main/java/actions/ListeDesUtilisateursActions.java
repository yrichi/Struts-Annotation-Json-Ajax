package actions;

import com.opensymphony.xwork2.ActionContext;
import modele.Exception.ExceptionUtilisateurNonExistant;
import modele.Personne;
import org.apache.struts2.convention.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


@Namespace("/user/listeUtilisateur")
@ResultPath(value="/")
@ParentPackage("json-default")
public class ListeDesUtilisateursActions  extends Environement{


    private String loginChangePwd;
    private String pwd;




    @Action(value="gotoPageListePersonne", results={
            @Result(name="success", location="listePersonneInscritDynamique.jsp")})
    public String goToPageListePersonne(){
        return SUCCESS;
    }

    @Action(value="ListePersonne", results={
            @Result(name="success",type = "json")})
public String listePersonneJson(){
        return SUCCESS;
}


    public List<Personne> getListePersonnes() {
        final List<Personne> personneList = new ArrayList<>();
        getGestion().listePersonne().values().stream().forEach(x->personneList.add((Personne) x)); // je prends ma map, je recup les valeur que je parcours , je caste les valeur et ajoute dans la liste que je renvoie
        return personneList;

    }


    @Action(value="change", results={
            @Result(name="success",type = "json")})
    public String changePassword(){
       // ActionContext.getContext().getParameters();
        try {
            getGestion().changePassword(loginChangePwd,pwd);
            return SUCCESS;
        } catch (ExceptionUtilisateurNonExistant exceptionUtilisateurNonExistant) {
            exceptionUtilisateurNonExistant.printStackTrace();
            return ERROR;
        }
    }


    public String getLoginChangePwd() {
        return loginChangePwd;
    }

    public void setLoginChangePwd(String loginChangePwd) {
        this.loginChangePwd = loginChangePwd;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
}
