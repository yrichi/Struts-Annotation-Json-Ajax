package actions;


import com.opensymphony.xwork2.ActionSupport;
import modele.Exception.ExceptionUtilisateurDejaExistant;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.ResultPath;

@Namespace("/user")
@Action(value="inscription", results={@Result(name="success",location="login.jsp"),
        @Result(name="error",location="inscription.jsp")})
@ResultPath(value="/")
public class InscriptionAction extends Environement{
    private String userName;
    private String pwd;
    private String pwd2;


    public String execute() {
        if (pwd.equals(pwd2)){
            try {
                getGestion().inscription(userName,pwd);
            } catch (ExceptionUtilisateurDejaExistant exceptionUtilisateurDejaExistant) {
                exceptionUtilisateurDejaExistant.printStackTrace();
                addFieldError("msgInscription","ce pseudo est deja utilise");
                return ERROR;
            }
        }else{ // message d'erreur mot de passe
            addFieldError("msgInscription","Vos mots de passes sont differents");
            return ERROR;
        }
        return SUCCESS;
    }

    @Action(value="gotoPageInscription", results={
            @Result(name="success",location="inscription.jsp")
    })
    public String goToPageInscription(){

        return SUCCESS;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getPwd2() {
        return pwd2;
    }

    public void setPwd2(String pwd2) {
        this.pwd2 = pwd2;
    }
}


