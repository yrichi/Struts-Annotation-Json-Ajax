package actions;

import modele.Exception.ExceptionMotDePasseIncorrect;
import modele.Exception.ExceptionUtilisateurDejaExistant;
import modele.Exception.ExceptionUtilisateurNonExistant;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.ResultPath;

import com.opensymphony.xwork2.ActionSupport;

@Namespace("/user")
@ResultPath(value="/")
	@Action(value="login", results={@Result(name="success",location="connecte.jsp"),
			@Result(name="error",location="login.jsp")})
public class LoginAction extends Environement{
	private String userName;
	private String pwd;
	public String execute() {
		try {
			getGestion().connexion(userName,pwd);
			return SUCCESS;
		} catch (ExceptionUtilisateurNonExistant exceptionUtilisateurNonExistant) {
			exceptionUtilisateurNonExistant.printStackTrace();
			addFieldError("msgConnexion","cet utilisateur n'existe pas");
			return ERROR;
		} catch (ExceptionMotDePasseIncorrect exceptionMotDePasseIncorrect) { // ne jamais indiquer en realite que le pseudo est bon et mdp incorect sinon probleme de securite
			addFieldError("msgConnexion","votre mot de passe est incorrect");
			exceptionMotDePasseIncorrect.printStackTrace();
			return ERROR;
		}

	}

	@Action(value="gotoPageConnexion", results={
			@Result(name="success",location="login.jsp")
	})
	public String gotoPageConnexion(){

		return SUCCESS;
	}



	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

}