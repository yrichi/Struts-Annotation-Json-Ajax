package modele.Exception;

public class ExceptionUtilisateurNonExistant extends Throwable {

    public ExceptionUtilisateurNonExistant() {
        super("L'utilisateur n'existe pas");
    }
}
