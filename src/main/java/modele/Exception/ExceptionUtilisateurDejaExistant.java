package modele.Exception;

public class ExceptionUtilisateurDejaExistant extends Throwable {
    public ExceptionUtilisateurDejaExistant(){
        super("L'utilisateur n'existe pas");
    }
}
