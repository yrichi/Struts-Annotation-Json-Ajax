package modele.Exception;

public class ExceptionMotDePasseIncorrect extends Throwable {

    public ExceptionMotDePasseIncorrect() {
        super("le mot de passe est incorrect");
    }
}
