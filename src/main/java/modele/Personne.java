package modele;

public class Personne {
    private String nom;
    private String password;

    public Personne(String nom, String prenom) {
        this.nom = nom;
        this.password = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
