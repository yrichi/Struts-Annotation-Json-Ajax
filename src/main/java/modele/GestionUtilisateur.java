package modele;

import modele.Exception.ExceptionMotDePasseIncorrect;
import modele.Exception.ExceptionUtilisateurDejaExistant;
import modele.Exception.ExceptionUtilisateurNonExistant;

import java.util.HashMap;
import java.util.Map;

public class GestionUtilisateur {



    private Map listePersonneInscrite = new HashMap<String,Personne>();

    {
        this.listePersonneInscrite.put("admin",new Personne("admin","admin"));
        this.listePersonneInscrite.put("user",new Personne("user","user"));
    }

    public void inscription(String nom, String motDePasse) throws ExceptionUtilisateurDejaExistant {
        if (listePersonneInscrite.get(nom) == null){
            listePersonneInscrite.put(nom,new Personne(nom,motDePasse));
        }else{
            throw new ExceptionUtilisateurDejaExistant();
        }
    }

    public void connexion(String login, String mdp) throws ExceptionUtilisateurNonExistant, ExceptionMotDePasseIncorrect {
        Personne p = (Personne) listePersonneInscrite.get(login);
        if (null != p){
            if (p.getPassword().equals(mdp)){
                return;
            }
            else {
                throw  new ExceptionMotDePasseIncorrect();
            }
        }else{
            throw new ExceptionUtilisateurNonExistant();
        }


    }


    public Map listePersonne(){
       return listePersonneInscrite;
    }

    public void changePassword(String loginChangePwd, String pwd) throws ExceptionUtilisateurNonExistant {
        if (listePersonneInscrite.get(loginChangePwd) == null){
            throw new ExceptionUtilisateurNonExistant();
        }else{
            Personne.class.cast(listePersonneInscrite.get(loginChangePwd)).setPassword(pwd);
        }
    }
}
