
$(document).ready(function(){
    var listePersonne;

    $("#formulairechangementmdp button").on('click',function (e) {
        e.preventDefault();
        e.stopPropagation();
        if(($("#mdp").val()!=$("#mdp2").val()) && $("#mdp").val()!="" ){
            alert("Le mot de passe est faux")
        }
        else{
            if ($("#formulairechangementmdp").attr("id")== null){
                alert ("clique sur utilisateur")
            }
            else{
                changePassword();
            }
        }
        return false;
    })

    getUtilisateurList();

    function getUtilisateurList() {
        $.ajax({
            type: "GET",
            url: "/user/listeUtilisateur/ListePersonne",
            success: function (data) {
                listePersonne = data['listePersonnes'];
                remplirUtilisateur(listePersonne)
            },
            error: function (data) {
                alert("Some error occured.");
            }
        });
    }

function remplirUtilisateur(data) {
        var listep ="";
    $.each(data, function(i) {
        listep += "<li id="+data[i]["nom"]+"> nom "+data[i]["nom"]+" mot de passe "+data[i]["password"]+"</li>";
    });
    $('#listePersonne').empty();
    $('#listePersonne').append(listep);

    $('#listePersonne li').click(function(e){
        selectionnerUsermodifierMotDePasse(e)
    });



}

function selectionnerUsermodifierMotDePasse(e) {
        $("#formulairechangementmdp").attr("idPseudo",e.currentTarget.id);
        $("#nameUserChangePassword").text(e.currentTarget.id);
        $("#formulairechangementmdp button").removeAttr("disabled")

}

    function changePassword(){
        var login =$("#formulairechangementmdp").attr("idPseudo");
        var pwd =$("#mdp").val();
        data =  {
            'loginChangePwd': login,
            'pwd': pwd // <-- the $ sign in the parameter name seems unusual, I would avoid it
        }
        $.ajax({
            type: "POST",
            data: data,
            url: "/user/listeUtilisateur/change.action",
            success: function (data) {
                listePersonne = data['listePersonnes'];
              remplirUtilisateur(listePersonne);
            },
            error: function (data) {
                alert("Some error occured.");
            }
        });
    }


});
