<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head></head>
<body>
<h1>Page Inscription</h1>

<s:form action="inscription">
	<s:textfield name="userName" label="pseudo"/>
	<s:password name="pwd" label="mot de passe "/>
	<s:password name="pwd2" label="meme mot de passe"/>
	<s:submit/>
</s:form>

<s:fielderror name="msgInscription"/>

<a href="<s:url action="gotoPageConnexion" namespace="/user">login</s:url>" >page de connexion</a>

</body>
</html>