<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head></head>
<body>
<h1>Page Connexion</h1>

<s:form action="login">
	<s:textfield name="userName" label="User Name"/>
	<s:password name="pwd" label="Password"/>
	<s:submit/>
</s:form>


<s:fielderror name="msgConnexion"/>

<a href="<s:url action="gotoPageInscription"></s:url>">lien inscription</a>

</body>
</html>